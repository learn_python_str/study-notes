
class Solution:
    def luck_number(self, matrix: list(list[int])) -> list[int]:
        ans = []
        for row in matrix:
            for j, x in enumerate(row):
                if max(r[j] for r in matrix) <= x <= min(row):
                    ans.append(x)
        return ans