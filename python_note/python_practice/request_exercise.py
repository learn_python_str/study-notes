# -*-coding:utf-8-*-
from json import JSONDecodeError

from lxml import html
import time

import execjs  # python 代码执行js代码库
import requests  # 网络请求库

import ddddocr

ocr = ddddocr.DdddOcr()


def use_time(func):
    def wrapped_function(*args, **kwargs):
        start_time = time.time()
        func(*args, **kwargs)
        end_time = time.time()
        print(f"总需要时间为：{end_time - start_time}")

    return wrapped_function


class ClassPublicInfo:

    def __init__(self, company_name=None, credit_no=None, personal_name=None, personal_id=None):
        self.headers = {}
        self.captchaId = None
        self.random_ = None
        self.ver = None  # 验证码
        # 公司
        self.company_name = company_name  # 公司名称
        self.credit_no = credit_no  # 社会信用代码/税号
        # 个人
        self.personal_name = personal_name  # 名字
        self.personal_id = personal_id  # 身份证号

    def get_captcha(self):
        """
        ::@describe: 拼接获取验证码url所需参数
        """
        ctx_01 = execjs.compile(
            """
            function getNum() {
                    var chars = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
                            'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
                            'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                            'x', 'y', 'z' ];
                    var nums = "";
                    for (var i = 0; i < 32; i++) {
                        var id = parseInt(Math.random() * 61);
                        nums += chars[id];
                    }
                    return nums;
                }
            """
        )
        captcha_id = ctx_01.call("getNum")

        ctx_02 = execjs.compile(
            """
            function getRandom() {
                    return Math.random();
                }
            """
        )
        _random = ctx_02.call("getRandom")
        self.captchaId = captcha_id
        self.random_ = _random
        url_captcha = f"http://zxgk.court.gov.cn/zhzxgk/captcha.do?captchaId={captcha_id}&random={_random}"
        return url_captcha

    def get_request_captcha(self, url):
        """
        :1、获取验证码
        :2、将验证码识别输出为字符串文本
        """
        self.headers['Proxy-Connection'] = 'keep-alive'
        self.headers['Pragma'] = 'no-cache'
        self.headers['Cache-Control'] = 'no-cache'
        self.headers[
            'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome' \
                            '/98.0.4758.82 Safari/537.36'
        self.headers['Referer'] = 'http://zxgk.court.gov.cn/zhzxgk/'
        self.headers[
            'Cookie'] = 'filePath=/2368/2019-05-16/06794bcb6950428bbc59fdc39cda1dee.pdf; JSESSIONID=1ED829A702A683' \
                        'C4CBE02FE02DA6CB5E; UM_distinctid=17efba2d5c5723-0f9c76319281a2-576153e-1fa400-17efba2d5c6eaa;' \
                        ' _gscu_15322769=44978003qwcoq692; _gscbrs_15322769=1; Hm_lvt_d59e2ad63d3a37c53453b99' \
                        '6cb7f8d4e=1644978003,1645413567; Hm_lpvt_d59e2ad63d3a37c53453b996cb7f8d4e=1645523357'
        response = requests.get(headers=self.headers, url=url)
        self.ver = ocr.classification(response.content)

    def get_check_ver(self):
        """
        :检查验证码是否打码正确:
        """
        url = f"http://zxgk.court.gov.cn/zhzxgk/checkyzm?captchaId={self.captchaId}&pCode={self.ver}"
        response = requests.get(headers=self.headers, url=url)
        text = response.text.strip()
        if text.isnumeric():
            if int(text) == 1:
                print("*" * 20 + f"码验证正确" + "*" * 20)
        else:
            print(f"text-------------{text}")
            raise

    def parse_second_data(self, date_str: str) -> dict:

        def get_td(td):
            if td.xpath('./text()|./strong/text()'):
                return td.xpath('./text()|./strong/text()')[0]
            else:
                return ''

        html_str = html.fromstring(date_str)
        ele_title_list = html_str.xpath(r'//div[@class=" col-lg-12  col-md-12 col-sm-12 bg-title"]/text()')
        ele_table_list = html_str.xpath(r'//table[@class="table table-condensed table-hover "]')
        trs = iter(ele_table_list)
        list_data = []
        for title in ele_title_list:
            try:
                tr = next(trs)
                tds = tr.xpath(r".//td")
                tds = list(map(get_td, tds))
                tds = list(map(lambda x: x.strip().replace("：", ""), tds))
            except StopIteration:
                raise
            if "查看限消令：" in tds:
                index_x = tds.index("查看限消令：")
                tds = tds[:index_x]
            if len(tds) % 2 == 0:
                dict_tds = dict(zip(tds[::2], tds[1::2]))
                json_data = {title: dict_tds}
                list_data.append(json_data)
        return list_data

    def get_second_data(self, item):
        url = "http://zxgk.court.gov.cn/zhzxgk/detailZhcx.do"
        params = {
            "pnameNewDel": item['pname'],
            "cardNumNewDel": "",
            "j_captchaNewDel": self.ver,
            "caseCodeNewDel": item['caseCode'],
            "captchaIdNewDel": self.captchaId,
        }
        self.headers['Content-Type'] = "application/x-www-form-urlencoded"
        response = requests.post(url=url, headers=self.headers, params=params)

        return self.parse_second_data(date_str=response.text)

    def get_first_info(self, page=1):
        url = "http://zxgk.court.gov.cn/zhzxgk/searchZhcx.do"
        self.headers['Origin'] = "http://zxgk.court.gov.cn"
        self.headers['Referer'] = "http://zxgk.court.gov.cn/zhzxgk/"
        self.headers['Content-Type'] = "application/x-www-form-urlencoded; charset=UTF-8"
        self.headers['X-Requested-With'] = "XMLHttpRequest"
        data = {
            "pName": self.company_name,
            "pCardNum": self.credit_no,
            "selectCourtId": "0",
            "pCode": self.ver,
            "captchaId": self.captchaId,
            "searchCourtName": "全国法院（包含地方各级法院）",
            "selectCourtArrange": "1",
            "currentPage": str(page),
        }
        response = requests.post(headers=self.headers, url=url, params=data)
        try:
            result_json = response.json()
        except JSONDecodeError as e:
            print(response.text)
            raise e
        result = result_json[0]['result']
        total_page = result_json[0]['totalPage']
        for item in result:
            # print(f"第一层数据：----{item['jsonObject']}")
            second_data = self.get_second_data(item)
            print(f"第二层数据：----{second_data}")
        if total_page > 1 and total_page > page:
            time.sleep(0.5)
            self.get_first_info(page + 1)
            if page >= 20:
                raise

    @use_time()
    def run(self):
        start_time = time.time()
        url = self.get_captcha()  # 获取captcha(获取验证码需要)
        self.get_request_captcha(url)  # 获取验证码
        self.get_check_ver()  # 检查验证码是否正确
        self.get_first_info()
        end_time = time.time()
        print(f"-------------{start_time-end_time}")



if __name__ == "__main__":
    company_name = "乐视网信息技术(北京)股份有限公司"
    credit_no = "911100007693890511"
    personal_name = None
    personal_id = None
    finder = ClassPublicInfo(company_name, credit_no, personal_name, personal_id)
    finder.run()
