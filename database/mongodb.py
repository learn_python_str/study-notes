import pymongo

# Replace the uri string with your MongoDB deployment's connection string.
conn_str = "mongodb://root:123456@192.168.247.135:27017/?authSource=admin"
# set a 5-second connection timeout
client = pymongo.MongoClient(conn_str, serverSelectionTimeoutMS=5000)
try:
    db = client.admin
    print(db.name)
    collection = db['students']
    student = {
        'id': '201701014',
        'name': 'Jordan',
        'age': 20,
        'gender': 'male'
    }
    result = collection.insert_one(student)
    print(db['students'].info)
except Exception as e:
    print("Unable to connect to the server.")
    print(f"-{e}")
