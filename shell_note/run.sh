:<<EOF
# 获取数组元素的个数
array_name=(value0 value1 value2 value3 value4)

echo 数组: ${array_name}

valuen=${array_name[n]}

echo valuen ${valuen}
# 获取数组元素的个数
length=${#array_name[@]}
echo $length
# 或者
length=${#array_name[*]}
echo $length

:<<EOF
注释
注

释
EOF

:<<EOF
echo shell 传递参数
echo "Shell 传递参数实例!"
echo "执行的文件名: $0";
echo "执行的第一个参数为：$1";
echo "执行的第二个参数为：$2";
echo "执行的第三个参数为：$3";
echo "传递到脚本的参数个数：$#";
echo "当前进程的ID号：$$";
echo "最后一个进程的ID好：$!";

EOF

:<<EOF
echo "--\$* 演示---"
for i in "$*"; do
	echo $i
done

echo "--\$@ 演示---"
for i in "$@"; do
	echo $i
done

EOF

echo shell 基本运算符

#加法
val=`expr 2 + 2`
echo "两数之和为：$val"

if [ -n "$1" ]; then
	echo "包括第一个参数"
else
	echo "没有包含第一个参数"
fi
