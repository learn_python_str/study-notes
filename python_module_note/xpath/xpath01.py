from lxml import etree, html

date_str = """





<!DOCTYPE html>
<html style="background:#eee">
<head>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no,email=no,address=no"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>全国法院信息综合查询 - 综合查询</title>
<link rel="icon" type="image/x-icon" href="static/img/favicon.ico" />
<link href="static/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="static/bootstrap/3.3.7/css/myset.css" rel="stylesheet">
<link href="static/css/selectropdown.css" rel="stylesheet">
<link href="static/css/index.css" rel="stylesheet">
<script type="text/javascript" src="static/bootstrap/3.3.7/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="static/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="static/javascript/data.js"></script>
<script type="text/javascript" src="static/javascript/dongfaZx.js"></script>

<style type="text/css">
.bg-title{
    font-size: 18px;
    font-weight: 600;
    color: white;
    background: #be3233;
    padding: 5px 25px;
    background: linear-gradient(to right,#B22222 ,#FFB6C1);
    margin-top: 20px;

}
.table {
    margin-bottom: 0px;
    border: 3px solid #eee;
}
td{
	border:2px solid #eee;
}

</style>
</head>
<body class="body-class">

<!-- <div class="header" style="background-image:url(static/img/headbg.png)"> -->
<!-- <header class="navbar navbar-static-top bs-docs-nav" id="top">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="http://zxgk.court.gov.cn" class="navbar-brand">中国执行信息公开网</a>
    </div>
    <nav id="bs-navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li>
           <a href = "http://zxgk.court.gov.cn/zhixing/" >全国被执行人公开</a>
        </li>
        <li class="active">
        	<a href = "http://zxgk.court.gov.cn/shixin/" >全国失信被执行人公开</a>
        </li>
        <li>
         	<a href = "http://zxgk.court.gov.cn/zhongben/">终本案件信息公开</a>
        </li>
        <li>
        	<a href = "http://zxgk.court.gov.cn/xgl/">限制消费人员信息公开</a>
        </li>
        <li>
        	<a href="http://wenshu.court.gov.cn/List/List?sorttype=1&conditions=searchWord+5+AJLX++%E6%A1%88%E4%BB%B6%E7%B1%BB%E5%9E%8B:%E6%89%A7%E8%A1%8C%E6%A1%88%E4%BB%B6">执行文书公开</a>
        </li>
        <li>
        	<a href="http://zxgk.court.gov.cn/zhzxgk/">执行综合查询</a>
        </li>
      </ul>
    </nav>
  </div>
</header> -->
<div class="header">
	<div class="container">
		<div class="row" style="padding-bottom: 10px">
				<div class="col-xs-12">
					<div id="head-title">
						<!-- <h1><span class="header-title">中国执行信息公开网</span></h1> -->
						<img alt="" src="static/img/lgoo.png">
					</div>
				</div>
				<div class="col-xs-12 ">
					<div style="float: right;" id="zxgk_link">
						<img src="static/img/btn_zhixinggongkaifuwu_nor.png"
							onmouseover="this.src='static/img/btn_zhixinggongkaifuwu_pre.png'"
							onmouseout="this.src='static/img/btn_zhixinggongkaifuwu_nor.png'">
						<div class="find-div-body" id="myDiv">
							<ul>
								<li><a href="http://zxgk.court.gov.cn/zhixing/">全国被执行人公开</a>
								</li>
								<li><a href="http://zxgk.court.gov.cn/shixin/">全国失信被执行人公开</a>
								</li>
								<li><a href="http://zxgk.court.gov.cn/zhongben/">终本案件信息公开</a>
								</li>

								<li><a href="http://zxgk.court.gov.cn/xgl/">限制消费人员信息公开</a>
								</li>
								<li><a
									href="http://wenshu.court.gov.cn/List/List?sorttype=1&conditions=searchWord+5+AJLX++%E6%A1%88%E4%BB%B6%E7%B1%BB%E5%9E%8B:%E6%89%A7%E8%A1%8C%E6%A1%88%E4%BB%B6">执行文书公开</a>
								</li>
								<li><a href="http://zxgk.court.gov.cn/zhzxgk/">执行综合查询</a></li>
							</ul>
						</div>
					</div>
					<div style="float: right;" id="home_link">
						<a href="http://zxgk.court.gov.cn/"> <img
							src="static/img/btn_home_nor.png"
							onmouseover="this.src='static/img/btn_home_pre.png'"
							onmouseout="this.src='static/img/btn_home_nor.png'">
						</a>
					</div>
				</div>
			</div>
	</div>
</div>
<div class="main grad" style="margin-bottom:20px">
	<div class="container" style="background:white;min-height:800px;">
		<div class="row">

				<div class="col-lg-12  col-md-12 col-sm-12 row-block">
				<div class=" col-lg-12  col-md-12 col-sm-12 bg-title">被执行人</div>
					<table class="table table-condensed table-hover ">
							<tr>
								<td height="20" width="200" align="right"><strong>被执行人姓名/名称：</strong><br /></td>
								<td id="pnameDetail" align="left">乐视网信息技术（北京）股份有限公司</td>
							</tr>
							<tr>
								<td height="20" align="right"><strong>身份证号码/组织机构代码：</strong></td>
								<td id="partyCardNumDetail" align="left">9111000076****0511</td>
							</tr>

								<tr>
									<td height="20" align="right"><strong>性别：</strong></td>
									<td id="Detail" align="left"> </td>
								</tr>

							<tr>
								<td height="20" align="right"><strong>执行法院：</strong></td>
								<td id="execCourtNameDetail" align="left">上海市浦东新区人民法院</td>
							</tr>
							<tr>
								<td height="20" align="right"><strong>立案时间：</strong></td>
								<td id="caseCreateTimeDetail" align="left">2022年01月20日</td>
							</tr>
							<tr>
								<td height="20" align="right"><strong>案号：</strong></td>
								<td id="caseCodeDetail" align="left">（2022）沪0115执2901号</td>
							</tr>
							<tr>
								<td height="20" align="right"><strong>执行标的：</strong></td>
								<td id="execMoneyDetail" align="left">23000</td>
							</tr>
						</table>
				</div>






				<div class="col-lg-2 col-lg-offset-5 col-md-4 clo-md-offset-4 col-sm-8  col-sm-offset-2 row-block">
					<button type="button" class="btn btn-zxgk btn-block" onclick="closeWindow()">关闭</button>
				</div>
		</div>
	</div>
</div>
<div class="bottom" id="bottom">
	<div class="row" style="margin-left:0;margin-right:0">
			<div class="col-lg-12" style="background:#be3333;color:white;">
				<span class="bottom-span">地址：北京市东城区东交民巷27号 </span>
				<span class="bottom-span">邮编：100745</span>
				<span class="moible-hide ">总机：010-67550114</span>
			</div>
			<div class="col-lg-12 moible-show">
				<span>总机：010-67550114</span>
			</div>
			<div class="col-lg-12">
				<span>中华人民共和国最高人民法院 版权所有</span>
			</div>
			<div class="col-lg-12">
				<span>京ICP备05023036号</span>
			</div>
		</div>
</div>
    <script>
        var contextPath = '';
        $(document).ready(function(){
      	   //点击隐藏/显示导航
      	   var myDiv = $("#myDiv");
      	    $("#zxgk_link").click(function(event) {
      	        // showDiv();//调用显示DIV方法
      	        $(myDiv).toggle();
      	        $(document).one("click",
      	        function() { //对document绑定一个影藏Div方法
      	            $(myDiv).hide();
      	        });
      	        event.stopPropagation(); //阻止事件向上冒泡
      	    });
      	    $(myDiv).click(function(event) {

      	        event.stopPropagation(); //阻止事件向上冒泡
      	    });
      	   });
        function closeWindow(){
        	window.close();
        }

        function testDisplaybzxr()
        {
            var divD = document.getElementById("bzxr");
            if(divD.style.display=="none")
            {
                divD.style.display = "table";
            }
            else
            {
                divD.style.display = "none";
            }
        }
        function testDisplaysx()
        {
            var divD = document.getElementById("sx");
            if(divD.style.display=="none")
            {

                divD.style.display = "table";
            }
            else
            {
                divD.style.display = "none";

            }
        }
        function testDisplayzb()
        {
            var divD = document.getElementById("zb");
            if(divD.style.display=="none")
            {
                divD.style.display = "table";
            }
            else
            {
                divD.style.display = "none";
            }
        }
        function testDisplayxgl()
        {
            var divD = document.getElementById("xgl");
            if(divD.style.display=="none")
            {
                divD.style.display = "table";
            }
            else
            {
                divD.style.display = "none";
            }
        }

        function showDetail(id ,pCode,captchaId,filePath,fileName){
        	if(filePath){
        		var sUserAgent = navigator.userAgent.toLowerCase();
        		var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
        		var url = "showPDF";
        		if(bIsIphoneOs){
        			post_self(url,[,"pCode","captchaId","filePath"],[pCode,captchaId,filePath]);
        		}else{
        			post(url,["pCode","captchaId","filePath","fileName"],[pCode,captchaId,filePath]);
        		}
        	}else{
        		alert("没有找到相关文书！")
        	}
        }
      //创建一个表单并提交（在新页面）
    	function post(URL, PARAMSNAME, PARAMS) {
		var temp_form = document.createElement("form");
		temp_form.action = URL;
		temp_form.target = "_blank";
		temp_form.method = "post";

		temp_form.style.display = "none";
		for ( var x in PARAMS) {
			var opt = document.createElement("textarea");
			opt.name = PARAMSNAME[x];
			opt.value = PARAMS[x];
			temp_form.appendChild(opt);
		}
		document.body.appendChild(temp_form);
		temp_form.submit();
	}
	function post_self(URL, PARAMSNAME, PARAMS) {
		var temp_form = document.createElement("form");
		temp_form.action = URL;
		temp_form.target = "_self";
		temp_form.method = "post";
		temp_form.style.display = "none";
		for ( var x in PARAMS) {
			var opt = document.createElement("textarea");
			opt.name = PARAMSNAME[x];
			opt.value = PARAMS[x];
			temp_form.appendChild(opt);
		}
		document.body.appendChild(temp_form);
		temp_form.submit();
	}
    	//ios设备判断
    	function isphone() {
    		var sUserAgent = navigator.userAgent.toLowerCase();
    		var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
    		var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
    		var bIsMidp = sUserAgent.match(/midp/i) == "midp";
    		var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
    		var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
    		var bIsAndroid = sUserAgent.match(/android/i) == "android";
    		var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
    		var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
    		if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid
    				|| bIsCE || bIsWM) {
    			return true
    		} else {
    			return false
    		}
    	}
    	//东法接口调用
    	dongfaZxwsbh('');


    </script>

</body>
</html>
				"""

html_str = html.fromstring(date_str)
ele_list = html_str.xpath(r'//div/table/tr/td/text()|//div/table/tr/td/strong/text()')
print(ele_list)
