#! /usr/bin/python
# -*- coding:utf-8 -*-

def hi(name='yasoob'):
    print("now you are in the hi() function")

    def greet():
        return "now you are in the greet() function"

    def welcome():
        return "now you are in the greet() function"

    print(greet())
    print(welcome())
    print("now you are back in the hi() function")

hi()
