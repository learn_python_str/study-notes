#!/user/bin/python
# -*- coding:UTF-8-*-

"""
@time:2022/01/20
"""

import asyncio
from pyppeteer import launcher, launch

# 去除浏览器自动化参数，隐藏自动化工具特征
launcher.DEFAULT_ARGS.remove("--enable-automation")

from python_module_note.pyppeteer.setting import *


async def screen_size():
    # 使用tkinter获取屏幕大小
    import tkinter
    tk = tkinter.Tk()
    width = tk.winfo_screenwidth()
    height = tk.winfo_screenheight()
    tk.quit()
    return width, height


async def main():
    start_parm = {
        'executablePath': dir_chrome,  # chrome执行路径
        'headless': False,
        'dumpio': True,
        'ignoreHTTPSErrors': False,  # 忽略证书错误
        'args': [
            '--disable-infobars',  # 关闭自动化提示框
            # '--no-sandbox',  # 关闭沙盒模式
            # '--window-size=1521.6,716',  # 窗口大小
            # '--start-maximized',  # 窗口大小
        ]
    }
    # 创建浏览器对象
    browser = await launch(**start_parm)
    # 创建一个页面对象
    context = await browser.createIncognitoBrowserContext()
    pages = await context.newPage()
    page = pages
    await page.setJavaScriptEnabled(True)
    # 设置ua
    await page.setUserAgent(
        'Mozilla/5.0 (Windows NT 100; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'
    )
    # 修改navigator.webdriver检测
    # 各种网站的检测js是不一样的，这是比较通用的。有的网站会检测运行的电脑运行系统，cpu核心数量，鼠标运行轨迹等等。
    # 反爬js
    js_text = """
        () =>{ 
        Object.defineProperties(navigator,{ webdriver:{ get: () => false } });window.navigator.chrome = { runtime: {},  };
        Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] });
        Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], });
         }
    """
    # 本页刷新后值不变，自动执行js
    await page.evaluateOnNewDocument(js_text)
    # 打开一个页面
    await page.setViewport({
        "width": 1521,
        "height": 716
    })
    await page.goto('https://etax.jiangxi.chinatax.gov.cn/etax/jsp/index.jsp')
    # 获取页面的cookies
    cookies = {'JX_ETAX_SID': 'iAA54GL4LKvVcoROqiMMhH1FHDm0f5EfkpKH4qDaNh9rltb0nbBi!1894527361!664666522'}
    # cookies = {'JX_ETAX_SID': 'TdwSC7AFz1GqWs_aT2ouT_QLm-dTh9ph1SEfbJLI4WuQh6jZ5zDU!927118939!-1530817105'}
    for k, v in cookies.items():
        cookies_new = {
            'url': 'https://etax.jiangxi.chinatax.gov.cn/',
            'name': k,
            'value': v
        }
        await page.setCookie(cookies_new)
    # 登录主页

    await page.goto('https://etax.jiangxi.chinatax.gov.cn/etax/jsp/business/ymtz/wycx_init.jsp')

    a = await page.querySelector('#barrierfree_container > div.header-wrap > div > a')
    print(f"------------a:{a}")
    await page.goto('https://etax.jiangxi.chinatax.gov.cn/etax/jsp/business/common/transition_ws_gz.jsp?sid=14010000',
                    waitUntil='networkidle2')
    await page.waitForNavigation()
    await asyncio.sleep(1.5)
    await page.click('#queryBtn')

    await asyncio.sleep(1.5)
    # 申报信息查询
    await page.goto('https://etax.jiangxi.chinatax.gov.cn/etax/jsp/portal/web_iframe.jsp?menucode=0401002')
    # # 查询增值税申报表
    iframes = page.frames
    await asyncio.sleep(.5)
    await iframes[1].evaluate(
        '''() => {
        document.getElementById("startDate").value='2019-01-01';
        doQuery()
        }'''
    )

    await asyncio.sleep(3)
    # 有可能没有任何申报信息
    try:
        await iframes[1].evaluate(
            '''() => {
                for (var i = 0; i < 10; i++) {
                    var obj = document.querySelector("#tbody > tr:nth-child(" + (i + 1) + ") > td:nth-child(3) > input");
                    if (obj != null) {
                        var text = obj.value;
                        if (text.indexOf('增值税及附加税费申报表') >= 0) {
                            document.querySelector("#tbody > tr:nth-child(" + (i + 1) + ") > td.read.left > a:nth-child(2)").click();
                            break;
                        }
                    }
                }
        }''')
    except:
        raise
    cookies = await page.cookies()
    cookies_str = '; '.join([cookie['name']+'='+cookie['value'] for cookie in cookies])
    print(cookies_str)
    await asyncio.sleep(10)

    await browser.close()


asyncio.get_event_loop().run_until_complete(main())
