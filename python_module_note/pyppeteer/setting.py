dir_chrome = '/opt/google/chrome/google-chrome'

start_parm = {
    'executablePath': dir_chrome,
    'headless': False,
    'dumpio': True,
    'ignoreHTTPSErrors': False,
    'args': [
        '--disable-infobars',  # 关闭自动化提示框
        # '--no-sandbox',  # 关闭沙盒模式
        '--start-maximized',  # 窗口大小
        '--window-size=1920,1080',  # 窗口大小
    ]
}


def screen_size():
    # 使用tkinter获取屏幕大小
    import tkinter
    tk = tkinter.Tk()
    width = tk.winfo_screenwidth()
    height = tk.winfo_screenheight()
    tk.quit()
    return width, height
