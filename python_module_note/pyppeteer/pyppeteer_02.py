#!/user/bin/python
# -*- coding:UTF-8-*-

"""
@time:2022/01/20
"""

import asyncio
from pyppeteer import launcher, launch

# 去除浏览器自动化参数，隐藏自动化工具特征
launcher.DEFAULT_ARGS.remove("--enable-automation")

from setting import *


async def main():
    start_parm = {
        'executablePath': dir_chrome,
        'headless': False,
        'dumpio': True,
        'autoClose': True,
        'ignoreHTTPSErrors': False,
        'args': [
            '--disable-infobars',  # 关闭自动化提示框
            # '--no-sandbox',  # 关闭沙盒模式
            '--start-maximized',  # 窗口大小
        ]
    }
    # 创建浏览器对象
    browser = await launch(**start_parm)
    # 创建一个页面对象
    pages = await browser.pages()
    page = pages[0]
    # 设置ua
    await page.setUserAgent(
        'Mozilla/5.0 (Windows NT 100; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'
    )
    # 修改navigator.webdriver检测
    # 各种网站的检测js是不一样的，这是比较通用的。有的网站会检测运行的电脑运行系统，cpu核心数量，鼠标运行轨迹等等。
    # 反爬js
    js_text = """
    () =>{ 
    Object.defineProperties(navigator,{ webdriver:{ get: () => false } });window.navigator.chrome = { runtime: {},  };
    Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] });
    Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], });
     }
    """
    url = "https://www.baidu.com/"
    # 本页刷新后值不变，自动执行js
    await page.evaluateOnNewDocument(js_text)
    # # 打开一个页面
    await page.goto(url)
    width, height = screen_size()
    print(width, height)
    await page.setViewport({'width': width-400, 'height': height})
    await asyncio.sleep(20)
    await browser.close()


asyncio.get_event_loop().run_until_complete(main())
