#!/user/bin/python
# -*- coding:UTF-8-*-

"""
@time:2022/01/20
"""

import asyncio
from pyppeteer import launcher, launch

# 去除浏览器自动化参数，隐藏自动化工具特征
launcher.DEFAULT_ARGS.remove("--enable-automation")

from setting import *

import asyncio
from pyppeteer import launch


async def main():
    browser = await launch(headless=False)
    # Create a new incognito browser context.
    # context = await browser.createIncognitoBrowserContext()
    # Create a new page in a pristine context.
    # page = await context.newPage()
    # Do stuff
    # await page.goto('https://example.com')
    await asyncio.sleep(30)
    # >>> {'width': 800, 'height': 600, 'deviceScaleFactor': 1}
    await browser.close()


asyncio.get_event_loop().run_until_complete(main())
