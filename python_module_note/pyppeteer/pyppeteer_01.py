"""
brief_introduction:简单通过pyppeteer来浏览百度首页进行截图保存
:1、
"""

import asyncio
from pyppeteer import launch


async def main():
    browser = await launch()
    page = await browser.newPage()
    await page.goto('https://www.baidu.com/')
    # await page.screenshot({'path': 'example.png'})
    await browser.close()


asyncio.get_event_loop().run_until_complete(main())
