import time
from numba import njit


@njit
def foo(x, y):
    s = 0
    for i in range(x, y):
        s += i
    return s


t1 = time.time()
print(foo(1, 100000000))
print('Time used: {} sec'.format(time.time() - t1))
