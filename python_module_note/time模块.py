#! /usr/bin/python
# -*- coding: UTF-8 -*-
import time

ticks = time.time()
localtime = time.localtime(time.time())
print(f"当前时间戳为:{ticks}")

print(f"本地时间为{localtime}")
print(f"本地时间为{localtime.tm_year}年；{localtime.tm_mon}月")
print(f"本地时间为{localtime.tm_year}年；{localtime.tm_mon}月；{localtime.tm_mday}号")
print(f"本地时间为{localtime.tm_year}年；{localtime.tm_mon}月；{localtime.tm_mday}号；{localtime.tm_hour}时")
print(f"本地时间为{localtime.tm_year}年；{localtime.tm_mon}月；{localtime.tm_mday}号；{localtime.tm_hour}时；{localtime.tm_min}分")
print(f"本地时间为{localtime.tm_year}年；{localtime.tm_mon}月；{localtime.tm_mday}号；{localtime.tm_hour}时；"
      f"{localtime.tm_min}分；{localtime.tm_sec}秒")


def get_week(tm_wday):
    dict_week = {
        0: "一",
        1: "二",
        2: "三",
        3: "四",
        4: "五",
        5: "六",
        6: "日",
    }
    return dict_week[tm_wday]


print(f"本地时间为星期{get_week(localtime.tm_wday)}")

format_localtime = time.asctime(time.localtime(time.time()))

print(f"本地时间为：{format_localtime}")
