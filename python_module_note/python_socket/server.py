# tcp服务端
from socket import *

IP = '192.168.31.40'  # 绑定ip地址
PORT = 5000  # 端口号

# 实例化一个socket对象
listenSocket = socket(AF_INET, SOCK_STREAM)

# 绑定ip和端口号
listenSocket.bind((IP, PORT))

# 最多接收五个客户端的链接
listenSocket.listen(5)

# 接收客户端连接，无客户端连接时，就处于睡眠状态
dataSocket, addr = listenSocket.accept()

# 尝试读取对方发送的消息
# BUFLEN 指定从接收缓冲里最多读取多少字节
while True:
    print(f"服务端已开启......")
    BUFLEN = 512  # 表示最多接受1024字节的数据
    recved = dataSocket.recv(BUFLEN)
    if not recved:
        break

    info = recved.decode()
    print(f'收到对方信息：{info}')

    dataSocket.send(f'服务端接收到了信息{info}'.encode())


#服务端也调用close（）关闭socket
dataSocket.close()
listenSocket.close()
