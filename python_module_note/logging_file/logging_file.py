import logging

# 1、将阀值设置为DEBUG
'''
logging.basicConfig(filename='example.log', level=logging.DEBUG)
logging.info(f"123")
logging.debug(f"1234")
logging.warning(f"12345")
'''

# 2、通过level参数获得你将传递给basicConfig(没有运行成功)
'''numeric_level = getattr(logging, loglevel.upper(), None)
if not isinstance(numeric_level, int):
    raise ValueError('Invalid log level: %s' % loglevel)

logging.basicConfig(level=numeric_level)

logging.basicConfig(filename='example.log', level=logging.DEBUG)
logging.info(f"123")
logging.debug(f"1234")
logging.warning(f"12345")'''
