import logging

# 1、更改用于显示消息的格式
'''logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logging.debug('123')
logging.info('1233')
logging.warning('1234')'''

# 2、显示事件的日期和时间
'''# logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p')
# 2021-12-31 11:15:07 AM is when this event was logged.
logging.warning(f'is when this event was logged.')'''

