# myapp.py

import logging
import mylib


# 3、从多个模块记录日志
def main():
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    logging.info('Started')
    mylib.do_something()
    logging.info('Finished')


if __name__ == "__main__":
    main()
