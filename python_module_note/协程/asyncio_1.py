import asyncio


async def main():
    print(f"hello")
    await asyncio.sleep(1)
    print('world')


asyncio.run(main())
