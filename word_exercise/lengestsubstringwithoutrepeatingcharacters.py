class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        mp = {}
        # res: 记录两个相同字符相隔多少
        # mx：记录子集中最长的字符串长度
        # bg：开始的字符的
        res, mx, bg = 0, 0, -1
        for i in range(len(s)):
            if s[i] not in mp or mp[s[i]] < bg:
                res += 1
                mx = max(res, mx)
                mp[s[i]] = i
            else:
                res = i - mp[s[i]]
                bg = mp[s[i]]
                mp[s[i]] = i
            # print(i, mp, res, mx, bg)
        return mx


A = Solution()
strs = A.lengthOfLongestSubstring('abcdabcbb')
print(strs)
