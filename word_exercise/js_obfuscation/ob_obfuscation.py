# -*- coding: utf-8 -*-
'''
无法100%还原为源代码，只能修改变量和方法名，增加可读性
'''
import re
import execjs

func_js = """
// 直接用重组后的数组替换原来的数组
var _0x30bb = ["Hello World!", "log"]

// 解混淆用到的函数
var _0xae0a = function (_0x38d89d, _0x30bbb2) {
    _0x38d89d = _0x38d89d - 0x0;
    var _0xae0a32 = _0x30bb[_0x38d89d];
    return _0xae0a32;
};
"""

# 1.编译解混淆函数到node.js环境中
js_func_name = '_0xae0a'  # 混淆js中函数定义的名称
ctx = execjs.compile(func_js)
# 2.正则匹配出所有需要替换的函数
with open('source.js') as f1:
    js = f1.read()

be_replaced_func_set = set(re.findall(js_func_name + "\([\s\S]+?\)",js))

print(be_replaced_func_set)
# 3.循环遍历进行替换
for be_replaced_func in be_replaced_func_set:
    args_tuple = re.findall("\(([\s\S]+?)\)", be_replaced_func)[0]
    args0 = eval(args_tuple.split(',')[0]) # 截取参数

    res = ctx.call(js_func_name, args0) # 调用参数，获取返回值
    js = js.replace(be_replaced_func, "'" + res + "'")
    print('{} 替换完成'.format(res))

with open('code.js', 'w') as f2: # 重写JS输出
    js = f2.write(js)

