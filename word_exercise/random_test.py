import asyncio
from pyppeteer import launch

async def login(username, password):
    browser = await launch({"headless": False})
    page = await browser.newPage()
    await page.goto("https://login.taobao.com/member/login.jhtml")
    await page.type("#fm-login-id", username)
    await page.type("#fm-login-password", password)
    await page.click("#J_SubmitStatic")
    # 等待登录成功
    await page.waitForSelector(".site-nav")

    # 打印登录后的用户信息
    user_info = await page.evaluate('''
        () => {
            const element = document.querySelector("span.site-nav-user");
            return element.innerText;
        }
    ''')
    print(f"登录成功！当前用户为：{user_info}")

    await browser.close()

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(login("YOUR_USERNAME", "YOUR_PASSWORD"))