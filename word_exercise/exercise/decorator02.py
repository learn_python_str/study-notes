import functools
from datetime import time


def timing(status='Train'):
    def dec(func):
        print('this is dec')

        @functools.wraps(func)
        def wrapper(*args,**kwargs):
            start = time.time()
            func1 = func(*args,**kwargs)   # 此处做了一个变形
            print('[%s] time: %.3f s '%(status,time.time()-start))
            return func1
        return wrapper
    return dec

@timing(status='Train')
def Training():
    time.sleep(3)

@timing(status='Test')
def Testing():
    time.sleep(2)


