def singleton(cls):
    # 创建一个字典用来保存类的实例对象
    _instance = {}

    def _singleton(*args, **kwargs):
        # 先判断这个类有没有对象
        if cls not in _instance:
            _instance[cls] = cls(*args, **kwargs)  # 创建一个对象,并保存到字典当中
        # 将实例对象返回
        return _instance[cls]

    return _singleton


@singleton
class Demo3(object):
    a = 1

    def __init__(self, x=0):
        self.x = x


a1 = Demo3(1)
a2 = Demo3(2)
print(id(a1))
print(id(a2))