#! /usr/bin/env python3

import cgi

header = 'Content-Type: text/html\n\n'

forhtml = '''
<HTML><HEAD><TITLE>
Friends CGI Demo</TITLE></HEAD>
<BODY><h3>Friends list for:<I>NEW USER</I></H3>
<FORM ACTION="/cgi-bin/friendsB.py">
<B>Enter You Name:</B>
<INPUT TYPE=text NAME=person, VALUE="NEW USER" SIZE=15>
<P><B>How many friends do you have?</B>
%S
</P><INPUT TYPE=submit></FORM></BODY></HTML>
'''

fradio = '<INPUT TYPE=radio NAME=howmany VALUE="%s" %s> %s \n'


def show_form():
    friends = []
    for i in (0, 10, 25, 50, 100):
        checked = ''
        if i == 0:
            checked = "CHECKED"
        friends.append(fradio % (str(i), checked, str(i)))
    print('%s%s' % (header, forhtml % ''.join(friends)))


reshtml = '''
<HTML><HEAD><TITLE>
Friends CGI Demo</TITLE></HEAD>
<BODY><H3>Friends list for: <I>%s</I></H3>
Your name is : <B>%s</B><P>
You have <B>%s</B><P> friends.
</BODY></HTML>
'''


def do_results(who, howmany):
    print(header + reshtml % (who, who, howmany))


def process():
    form = cgi.FieldStorage()
    if 'person' in form:
        who = form['person'].value
    else:
        who = 'NEW USER'
    if 'howmany' in form:
        howmany = form['howmany'].value
    else:
        howmany = 0
    if 'action' in form:
        do_results(who, howmany)
    else:
        show_form()


if __name__ == "__main__":
    process()
