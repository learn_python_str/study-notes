#!/user/bin/env python

import _thread
from time import sleep, ctime

loops = [4, 2]


def loop(nloop, nsec, lock):
    print('start loop' + str(nloop) + 'at' + ctime())
    sleep(nsec)
    print('loop' + str(nloop) + 'done at' + ctime())
    lock.release()


def main():
    print('starting at:' + ctime())
    locks = []
    nloops = range(len(loops))
    # print("-------" + len(loops) + "-------")

    for i in nloops:
        lock = _thread.allocate_lock()
        lock.acquire()
        locks.append(lock)

    for i in nloops:
        _thread.start_new_thread(loop, (i, loops[1], locks[i]))

    for i in nloops:
        while locks[i].locked():
            pass


if __name__ == "__main__":
    main()
