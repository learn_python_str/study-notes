class Singleton(object):
    def __new__(self, *args, **kwargs):
        if not hasattr(self, '_instance'):
            self._instance = super().__new__(self, *args, **kwargs)
        return self._instance


class MyClass(Singleton):
    a = 1


one = MyClass()
two = MyClass()

print(id(one))
print(id(two))
print(one == two)
print(one is two)
