class Singleton(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        print(hasattr(cls, '_instance'))
        if hasattr(cls, '_instance'):
            cls._instance = super().__new__(cls, *args, **kwargs)
        else:
            return cls._instance


class Demo1(object):
    def __new__(cls, *args, **kwargs):
        #  判断单例是否存在：_instance属性中存储的就是单例
        if not hasattr(cls, '_instance'):
            # 如果单例不存在，初始化单例
            cls._instance = super().__new__(cls, *args, **kwargs)
        # 返回单例
        return cls._instance


singleton1 = Demo1()
singleton2 = Demo1()
print(id(singleton2))
print(id(singleton1))
print(singleton1.__str__())