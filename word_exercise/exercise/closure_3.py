def outer_fun():
    x = 'a'

    def inner_fun():
        x = 'b'
        print('inner x:', x, 'at', id(x))

    print('outer x before call inner:', x, 'at', id(x))
    inner_fun()
    print('outer x before call inner:', x, 'at', id(x))


outer_fun()