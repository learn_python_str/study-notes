

# def foo():
# 	def bar():
# 		print('bar() called')
# 	print('foo() called')
# 	bar()
#
#
# foo()

# def foo():
# 	def bar():
# 		print('bar() called')
# 	print ('foo() called')
# 	return bar()
#
# foo()
# import functools
#
#
# def dec(func):
# 	@functools.wraps(func)  # 加这句是为了防止装饰器对被装饰函数的影响
# 	def wrapper(*args, **kwargs):
# 		print('this is a wrapper')
# 		return func(*args, **kwargs)
#
# 	return wrapper
#
#
# @dec
# def foo():
# 	print('foo() called')
#
import functools


def dec(func):
	print('this is dec')

	@functools.wraps(func)  # 加这句是为了防止装饰器对被装饰函数的影响
	def wrapper(*args, **kwargs):
		print('this is a wrapper')
		return func(*args, **kwargs)

	return wrapper


@dec
def foo():
	print('foo() called')




