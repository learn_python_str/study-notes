def num():
    return [lambda x: x * i for i in range(4)]


print([m(2) for m in num()])

for m in num():
    print(m(2))
