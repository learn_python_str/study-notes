#!/user/bin/env python

import threading
import time
from time import sleep, ctime

loops = [2, 4]


class ThreadFunc(object):

    def __init__(self, func, args, name=''):
        self.func = func
        self.name = name
        self.args = args

    def __call__(self):
        self.func(*self.args)


def loop(nloop, nesc):
    print('start loop' + str(nloop) + 'at' + ctime())
    time.sleep(nesc)
    print('end loop' + str(nloop) + 'at' + ctime())


def main():
    print('thread start at' + ctime())
    p = range(len(loops))
    thread = []
    for i in p:
        t = threading.Thread(target=ThreadFunc(loop, (i, loops[i]), loop.__name__))
        thread.append(t)

    for i in p:
        thread[i].start()

    for i in p:
        thread[i].join()

    print('thread end at' + ctime())


if __name__ == '__main__':
    main()
