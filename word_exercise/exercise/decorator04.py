import time


class LogTime2:
    def __init__(self, use_int=False):
        self.use_int = use_int

    def __call__(self, func):  # 接受一个函数作为参数
        def _log(*args, **kwargs):
            beg = time.time()
            res = func(*args, **kwargs)
            if self.use_int:
                print('use time: {}'.format(int(time.time() - beg)))
            else:
                print('use time: {}'.format(time.time() - beg))
            return res

        return _log


@LogTime2(True)
def mysleep4():
    time.sleep(1)


mysleep4()


@LogTime2(False)
def mysleep5():
    time.sleep(1)


mysleep5()