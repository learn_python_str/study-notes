# 定义一个指定范围的自然数类，并可以提供迭代
class Num:
    def __init__(self, max_num):
        self.max_num = max_num
        self.count = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.count < self.max_num:
            self.count += 1
            return self.count
        else:
            raise StopIteration('已经到达临界')


num = Num(10)
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))