def tag(tag_name):
    def add_tag(context):
        return "<{1}>{0}</{1}>".format(context, tag_name)

    return add_tag


context = 'hello'
add_tag = tag('a')
print(add_tag(context))
