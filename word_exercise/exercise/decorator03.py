import functools
from datetime import time


def timing(status='Train'):
    print('this is timing')

    def dec(func):
        print('this is dec in timing')

        @functools.wraps(func)
        def wrapper3(*args, **kwargs):
            start = time.time()
            func1 = func(*args, **kwargs)
            print('[%s] time: %.3f s ' % (status, time.time() - start))
            return func1

        return wrapper3

    return dec


def dec1(func):
    print('this is dec1')

    @functools.wraps(func)
    def wrapper1(*args, **kwargs):
        print('this is a wrapper in dec1')
        return func(*args, **kwargs)

    return wrapper1


def dec2(func):
    print('this is dec2')

    @functools.wraps(func)
    def wrapper2(*args, **kwargs):
        print('this is a wrapper in dec2')
        return func(*args, **kwargs)

    return wrapper2


@dec1
@dec2
@timing(status='Test')
def fun():
    time.sleep(2)

