from itertools import accumulate
#
# arr = [1, 2, 3, 4, 5, 6]
#
#
# def fun(a, b):
#     # 乘法函数
#     return a * b
#
#
# res1 = list(accumulate(arr))
# print(len(res1))
# print(res1)
from typing import List


class Solution:
    def maxSumTwoNoOverlap(self, nums: List[int], firstLen: int, secondLen: int) -> int:
        s = list(accumulate(nums, initial=0))  # nums 的前缀和
        ans = 0

        def f(firstLen: int, secondLen: int) -> None:
            nonlocal ans
            maxSumA = 0
            for i in range(firstLen + secondLen, len(s)):
                maxSumA = max(maxSumA, s[i - secondLen] - s[i - secondLen - firstLen])
                ans = max(ans, maxSumA + s[i] - s[i - secondLen])

        f(firstLen, secondLen)  # 左 a 右 b
        f(secondLen, firstLen)  # 左 b 右 a
        return ans


a = Solution()
nums = [0, 6, 5, 2, 2, 5, 1, 9, 4]
firstLen = 1
secondLen = 2
b = a.maxSumTwoNoOverlap(nums, firstLen, secondLen)
print(b)