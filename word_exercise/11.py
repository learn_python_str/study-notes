from threading import Thread
import threading
import os, time

loop = 10000000
number = 0


# 计算密集型任务
def _add(count):
    global number
    for i in range(count):
        number += 1


def _sub(count):
    global number
    for i in range(count):
        number -= 1


if __name__ == "__main__":
    l = []
    print("本机为", os.cpu_count(), "核 CPU")  # 本机为64核
    start = time.time()
    t1 = Thread(target=_add, args=(loop,))
    t2 = Thread(target=_sub, args=(loop,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    print(number)
