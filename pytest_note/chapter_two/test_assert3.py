import logging

import pytest

logger = logging.getLogger(__name__)


def test_recursion_depth():
    with pytest.raises(RuntimeError) as excinfo:
        def f():
            f()

        f()
    logger.info(excinfo.value)
    assert "maximum recursion" in str(excinfo.value)
